export default () => ({
    participants: [{
            name: 'Văn Cương',
            id: 1,
            profilePicture: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/NafSadh_Profile.jpg/768px-NafSadh_Profile.jpg'
        },
        {
            name: 'Xuân Toản',
            id: 2,
            profilePicture: 'https://lh3.googleusercontent.com/-G1d4-a7d_TY/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJPez_wX5UCJztzEUeCxOd7HBK7-jA.CMID/s83-c/photo.jpg'
        }
    ],
    myself: {
        name: 'SangPT',
        id: 3,
        profilePicture: 'https://lh3.googleusercontent.com/ogw/ADGmqu-Gy8CqpSuvmtte9evH-DJ9-lowo3E78ld-0flMJw=s83-c-mo'
    },
    messages: [{
            content: 'hi',
            myself: false,
            participantId: 1,
            timestamp: { year: 2019, month: 3, day: 5, hour: 20, minute: 10, second: 3, millisecond: 123 },
            type: 'text'
        },
        {
            content: '123',
            myself: true,
            participantId: 3,
            timestamp: { year: 2019, month: 4, day: 5, hour: 19, minute: 10, second: 3, millisecond: 123 },
            type: 'text'
        },
        {
            content: 'gg',
            myself: false,
            participantId: 2,
            timestamp: { year: 2019, month: 5, day: 5, hour: 10, minute: 10, second: 3, millisecond: 123 },
            type: 'text'
        }
    ],
    chatTitle: 'Group Chat',
    toLoad: [{
            content: 'Hey, ',
            myself: false,
            participantId: 2,
            timestamp: { year: 2011, month: 3, day: 5, hour: 10, minute: 10, second: 3, millisecond: 123 },
            uploaded: true,
            viewed: true,
            type: 'text'
        },
        {
            content: "evening.",
            myself: true,
            participantId: 3,
            timestamp: { year: 2010, month: 0, day: 5, hour: 19, minute: 10, second: 3, millisecond: 123 },
            uploaded: true,
            viewed: true,
            type: 'text'
        },
    ],
})